DevOps+CDA Handler
===========================

This handler enables both:

-  :doc:`DevOps Handler<devopshandler>`, base of
   :doc:`SSO as a service (SSOaaS)<ssoaas>`
-  :doc:`CDA<cda>`, cross domain authentication mechanism
