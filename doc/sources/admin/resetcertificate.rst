Certificate reset
=================

Presentation
------------

This plugin allows users to reset their certificate informations.

**Kinematics:**

-  User click reset certificate button.
-  He enters his mail.
-  LL::NG looks for the user in users database with given information.
-  An email with a link is sent if user exists.
-  User clicks on the link and he is redirected to the portal.
-  The portal asks him to upload his certificate file (base64, pem
   only).
-  A confirmation mail is sent to confirm the certificate has been
   successfully reset.


.. danger::
      LDAP backend supported only



Configuration
-------------

**Requirements**

-  You have to activate the 'certificate reset' button in the login page.
   Go in Manager, ``General Parameters`` » ``Portal`` » ``Customization`` » ``Buttons on login page`` » ``Reset certificate``.
   See :ref:`portal customization<portalcustom-other-parameters>`.

-  The SMTP server must be setup. See :doc:`SMTP server setup<smtp>`.

-  The register module also must be setup.
   Go in Manager, ``General Parameters`` » ``Authentication parameters`` » ``Register Module`` and choose your module.


**Manager Configuration**

Go in Manager, ``General Parameters`` » ``Plugins`` » ``Certificate management``:

-  **Mail content:**

   -  **Certificat reset mail subject**: subject of mail sent when certificate is reset
   -  **Certificat reset mail content** (optional): content of mail sent when certificate is reset
   -  **Confirmation mail subject**: subject of mail sent when certificate reset is asked
   -  **Confirmation mail content** (optional): content of mail sent when certificate is asked


.. attention::

    By default, mail contents are empty in order to use templates:

    -  portal/skins/common/mail_certificateConfirm.tpl
    -  portal/skins/common/mail_certificateReset.tpl

    If you define custom mail contents in Manager, then templates won't be
    used.

-  **Other:**

   -  **Reset certificate page URL**: URL of certificate reset page (default: [PORTAL]/certificateReset)
   -  **Certificate descrition attribute**: attribute where to save certificate description name (Default description)
   -  **Certificate hash attribute**: attribute where to store certificate hash (Default userCertificate;binary)
   -  **Minimun duration before expiration**: number of days of validity before certificate expires. Default 0.


.. danger::
   .p12 certificates only.



.. |image0| image:: /documentation/beta.png
   :width: 100px
