# lemonldap-ng po-debconf translation to Spanish
# Copyright (C) 2010 Software in the Public Interest
# This file is distributed under the same license as the lemonldap-ng package.
#
# Changes:
# - Initial translation
# Camaleón <noelamac@gmail.com>, 2010
#
# - Updates
#
#
# Traductores, si no conocen el formato PO, merece la pena leer la
# documentación de gettext, especialmente las secciones dedicadas a este
# formato, por ejemplo ejecutando:
# info -n '(gettext)PO Files'
# info -n '(gettext)Header Entry'
#
# Equipo de traducción al español, por favor lean antes de traducir
# los siguientes documentos:
#
# - El proyecto de traducción de Debian al español
# http://www.debian.org/intl/spanish/
# especialmente las notas y normas de traducción en
# http://www.debian.org/intl/spanish/notas
#
# - La guía de traducción de po's de debconf:
# /usr/share/doc/po-debconf/README-trans
# o http://www.debian.org/intl/l10n/po-debconf/README-trans
#
msgid ""
msgstr ""
"Project-Id-Version: lemonldap-ng 1.0.2\n"
"Report-Msgid-Bugs-To: lemonldap-ng@packages.debian.org\n"
"POT-Creation-Date: 2010-12-04 23:10+0100\n"
"PO-Revision-Date: 2010-12-09 08:56+0100\n"
"Last-Translator: Camaleón <noelamac@gmail.com>\n"
"Language-Team: Debian Spanish <debian-l10n-spanish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-Language: Spanish\n"

#. Type: string
#. Description
#: ../liblemonldap-ng-common-perl.templates:1001
msgid "LDAP server:"
msgstr "Servidor LDAP:"

#. Type: string
#. Description
#: ../liblemonldap-ng-common-perl.templates:1001
msgid ""
"Set here name or IP address of the LDAP server that has to be used by "
"Lemonldap::NG. You can modify this value later using the Lemonldap::NG "
"manager."
msgstr ""
"Introduzca el nombre o la dirección IP del servidor LDAP que usará "
"Lemonldap::NG. Podrá modificar este valor utilizando el administrador de "
"Lemonldap::NG."

#. Type: string
#. Description
#: ../liblemonldap-ng-common-perl.templates:2001
msgid "Lemonldap::NG DNS domain:"
msgstr "Dominio DNS de Lemonldap::NG:"

#. Type: string
#. Description
#: ../liblemonldap-ng-common-perl.templates:2001
msgid ""
"Set here the main domain protected by Lemonldap::NG. You can modify this "
"value later using the Lemonldap::NG manager."
msgstr ""
"Introduzca el dominio principal protegido por Lemonldap::NG. Podrá modificar "
"este valor desde el administrador de Lemonldap::NG."

#. Type: string
#. Description
#: ../liblemonldap-ng-common-perl.templates:3001
msgid "Lemonldap::NG portal:"
msgstr "Portal de Lemonldap::NG:"

#. Type: string
#. Description
#: ../liblemonldap-ng-common-perl.templates:3001
msgid ""
"Set here the Lemonldap::NG portal URL. You can modify this value later using "
"the Lemonldap::NG manager."
msgstr ""
"Introduzca la URL del portal de Lemonldap::NG. Podrá modificar este valor "
"desde el administrador de Lemonldap::NG."

#. Type: string
#. Description
#: ../liblemonldap-ng-common-perl.templates:4001
msgid "LDAP server port:"
msgstr "Puerto del servidor LDAP:"

#. Type: string
#. Description
#: ../liblemonldap-ng-common-perl.templates:4001
msgid ""
"Set here the port used by the LDAP server. You can modify this value later "
"using the Lemonldap::NG manager."
msgstr ""
"Introduzca el puerto que usará el servidor LDAP. Podrá modificar este valor "
"desde el administrador de Lemonldap::NG."

#. Type: string
#. Description
#: ../liblemonldap-ng-common-perl.templates:5001
msgid "LDAP search base:"
msgstr "Base de búsqueda LDAP:"

#. Type: string
#. Description
#: ../liblemonldap-ng-common-perl.templates:5001
msgid ""
"Set here the search base to use in LDAP queries. You can modify this value "
"later using the Lemonldap::NG manager."
msgstr ""
"Introduzca la base de búsqueda para las consultas LDAP. Podrá modificar este "
"valor desde el administrador de Lemonldap::NG manager."

#. Type: string
#. Description
#: ../liblemonldap-ng-common-perl.templates:6001
msgid "LDAP account:"
msgstr "Cuenta LDAP:"

#. Type: string
#. Description
#: ../liblemonldap-ng-common-perl.templates:6001
msgid ""
"Set here the account that Lemonldap::NG has to use for its LDAP requests. "
"Leaving it blank causes Lemonldap::NG to use anonymous connections. You can "
"modify this value later using the Lemonldap::NG manager."
msgstr ""
"Introduzca la cuenta que usará Lemonldap::NG para las solicitudes LDAP. Si "
"lo deja en blanco, Lemonldap::NG utilizará conexiones anónimas. Podrá "
"modificar este valor desde el administrador de Lemonldap::NG."

#. Type: string
#. Description
#: ../liblemonldap-ng-common-perl.templates:7001
msgid "LDAP password:"
msgstr "Contraseña LDAP:"

#. Type: string
#. Description
#: ../liblemonldap-ng-common-perl.templates:7001
msgid ""
"Set here the password for the Lemonldap::NG LDAP account. You can modify "
"this value later using the Lemonldap::NG manager."
msgstr ""
"Introduzca la contraseña de la cuenta LDAP de Lemonldap::NG. Podrá modificar "
"este valor desde el administrador de Lemonldap::NG."

#. Type: boolean
#. Description
#: ../liblemonldap-ng-common-perl.templates:8001
msgid ""
"Lemonldap::NG configuration files have changed, try to migrate your files?"
msgstr ""
"Los archivos de configuración de Lemonldap::NG han cambiado. ¿Desea intentar "
"migrar sus archivos?"
